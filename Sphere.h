//
// Created by ice-tea on 04.09.18.
//

#ifndef PROJECT_SPHERE_H
#define PROJECT_SPHERE_H

#endif //PROJECT_SPHERE_H

#include "Simple_object.h"
#include <cmath>

class Sphere : Simple_object {
private:
    const double RESISTANCE = 0.47;

    int radius;

public:
    inline double get_volume() override;
};