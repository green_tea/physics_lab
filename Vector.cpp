//
// Created by ice-tea on 04.09.18.
//

#include "Vector.h"

Vector::Vector(double x, double y) {
    this->x = x;
    this->y = y;
}

Vector::Vector() : Vector(0, 0) {}

Vector::Vector(double a) : Vector(a, a) {}

Vector::Vector(int a) : Vector(a, a) {}

const Vector& operator+(const Vector& lhs, const Vector &rhs) const {
    return Vector(lhs.x + rhs.x, lhs.y + rhs.y);
}

const Vector& operator/(const Vector& lhs, const Vector& rhs) const  {
    return Vector(lhs.x / rhs.x, lhs.y / rhs.y);
}

const Vector& operator*(const Vector& lhs, const Vector& rhs) const {
    return Vector(lhs.x * rhs.x, lhs.y * rhs.y);
}
