//
// Created by ice-tea on 29.08.18.
//

#include "Simple_object.h"

Simple_object::Simple_object(int density, int start_x, int start_y) {
    this->cur_x = start_x;
    this->cur_y = start_y;
    this->density = density;
}

void Simple_object::move(const Vector& impulse) {
    this->speed = impulse + this->speed;
}

Simple_object::Simple_object() : Simple_object(0, 0, 0) {}

double Simple_object::get_mass() {
    return this->get_volume() * this->density;
}

inline const Vector& Simple_object::get_archimed_force() {
    return Vector(0, std::round(this->density * G * this->get_volume()));
}

inline const Vector& Simple_object::get_gravity_force() {
    return Vector(0, -this->get_mass() * G);
}

inline const Vector& Simple_object::get_resistance_force() {
    double temp = -0.5 * sqrt(this->speed.x * this->speed.x + this->speed.y * this->speed.y) * RESISTANCE * this->get_Square() * this->density;
    return Vector(this->speed.x * temp, this->speed.y * temp);
}

inline const Vector& Simple_object::get_sum_of_forces() {
    return get_archimed_force() + get_gravity_force() + get_resistance_force();
}

inline void Simple_object::recalc_speed() {
    speed = speed + acceleration * TICK_TIME;
}

inline void Simple_object::recalc_acceleration() {
    acceleration = get_sum_of_forces() / get_mass();
}

void Simple_object::next_tick() {
    recalc_acceleration();
    recalc_speed();
    cur_x += speed.x;
    cur_y += speed.y;
}