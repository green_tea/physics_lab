cmake_minimum_required(VERSION 2.8)
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
add_executable(main main.cpp Simple_object.cpp Simple_object.h Vector.h Vector.cpp Sphere.cpp Sphere.h Constants.h)