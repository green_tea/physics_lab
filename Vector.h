//
// Created by ice-tea on 03.09.18.
//

#ifndef PROJECT_VECTOR_H
#define PROJECT_VECTOR_H

#endif //PROJECT_VECTOR_H

class Vector{
public:
    double x, y;

    Vector(double x, double y);

    Vector(double a);

    Vector(int a);

    Vector();

    friend const Vector&operator+(const Vector& lhs, const Vector& rhs) const;

    friend const Vector&operator*(const Vector& lhs, const Vector& rhs) const;

    friend const Vector&operator/(const Vector& lhs, const Vector& rhs) const;
};