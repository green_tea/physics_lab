//
// Created by ice-tea on 29.08.18.
//

#ifndef PROJECT_SIMPLE_OBJECT_H
#define PROJECT_SIMPLE_OBJECT_H

#include "Vector.h"
#include "Constants.h"
#include <cmath>


class Simple_object {
private:

    const double TICK_TIME = 0.0001;
    const double RESISTANCE = 0;

    int density;
    int cur_x, cur_y;
    Vector speed, acceleration;

    inline const Vector& get_archimed_force();

    inline const Vector& get_gravity_force();

    inline const Vector& get_resistance_force();

    inline double get_mass();

    inline const Vector& get_sum_of_forces();

    inline void recalc_speed();

    inline void recalc_acceleration();

protected:
    int volume;

    virtual inline double get_Square() = 0;

    virtual inline double get_volume() = 0;

public:
    Simple_object(int density, int start_x, int start_y);

    Simple_object();

    void move(const Vector& impulse);

    void next_tick();
};


#endif //PROJECT_SIMPLE_OBJECT_H
