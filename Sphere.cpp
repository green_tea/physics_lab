//
// Created by ice-tea on 04.09.18.
//

#include "Sphere.h"

inline double Sphere::get_volume() {
    return M_PI * this->radius * this->radius;
}